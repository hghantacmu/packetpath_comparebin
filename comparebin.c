/*
 * Author : Harsha vardhan
 * College : Carnegie Mellon University
 * Email: hghanta@andrew.cmu.edu,4124788034
 */

/*
 * The program uses memory mapping to reduce the overhead that would have otherwise caused by normal
 * unix I/O or buffered I/O functions. The file names are taken from the user as the input using getopt library.
 * It uses a 'Binary search' type of procedure to recursively find the point of difference between files.
 * It also handles the cases if two files are of not equal size or empty
 */
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <math.h>
#include <stdbool.h>
#include <errno.h>

#define NMSEC_PER_SEC 1000
#define NULL_CHAR "\0"
#define CHAR_SIZE 8

/*Forward declartion of the helper functions */
long min(long num1, long num2);
char ** findDiffPoint(char *p1,char *p2,long size);
bool isEnd(char *p,char *fp,long size);
void printDiffChars(char *p,char *fp,long size);

int main(int argc, char **argv){
    bool equal=false;
    struct stat sb1;
    struct stat sb2;

    char *file1="\0";
    char *file2="\0";
    int option;
    int count=0;
    char *help= "Command usage : comparebin -a -b  \n-a : path of the first file \n-b : path of the "
                "second file \n-h : print this message\n";
    while((option=getopt(argc,argv,"a:b:h"))!=-1){

        switch(option){
            case 'a' :
                file1=malloc(sizeof(char) * strlen(optarg));
                strncpy(file1,optarg,strlen(optarg));
                count++;
                break;
            case 'b':
                file2=malloc(sizeof(char)*strlen(optarg));
                strncpy(file2,optarg,strlen(optarg));
                count++;
                break;
            case 'h':
                printf("%s",help);
                break;
            default:
                printf("Invalid options \n");
                printf("%s",help);
                _exit(EXIT_FAILURE);
        }
    }

    if(count!=2) {
        if (!strncmp(file2,NULL_CHAR,1)) printf("Second file is not entered \n");
        if (!strncmp(file1,NULL_CHAR,1)) printf("fist file is not entered\n");
        _exit(EXIT_SUCCESS);
    }
    clock_t  start,end;
    start=clock();

    int f1,f2;
    if((f1=open(file1,O_RDONLY))<0) {
        perror("Problem opening file");
        exit(EXIT_FAILURE);
    }
    if((f2=open(file2,O_RDONLY))<0){
        perror("Problem opening file");
        exit(EXIT_FAILURE);
    }
    if(fstat(f1,&sb1)==-1) {
        perror("Could not get the size of file");
        exit(EXIT_FAILURE);
    }
    if(fstat(f2,&sb2)==-1) {
        perror("Could not get the size of file");
        exit(EXIT_FAILURE);
    }
    long size1=sb1.st_size;
    long size2=sb2.st_size;
    if(size1==0 || size2==0) {
        printf("Empty files\n");
        exit(EXIT_FAILURE);
    }
    if(size1!=size2) {
        printf("The size of two files is not equal.First :%f Kb,second: %f Kb\n",(double)size1/1024,(double)size2/1024);
    }else{
        printf("Two files are of equal size .First :%ld Kb,second: %ld Kb\n",size1/(1024),size2/(1024));
    }

    long minSize=min(size1,size2);
    char *map1=NULL;
    char *map2=NULL;
    if((map1 = mmap(NULL,size1,PROT_READ,MAP_PRIVATE,f1,0))<0){
        close(f1);
        close(f2);
        perror("Error mapping the file to memory\n");
        exit(EXIT_FAILURE);
    }
    if((map2 = mmap(NULL,size2,PROT_READ,MAP_PRIVATE,f2,0))<0){
        close(f1);
        close(f2);
        munmap(map1,size1);
        perror("Error mapping the second file to memory\n");
        exit(EXIT_FAILURE);
    }

    char *t1=map1;
    char *t2=map2;
    char **neq=findDiffPoint(t1,t2,minSize);
    if(neq==NULL && size1!=size2) {
        //if the size is not equal but no diff point is detected , that means one file is the super set of another
        if(size1>size2){
            printf("First file is a super set of second file and first 16 bytes of extra chars are..\n");
            printDiffChars(t1+size2,t1,size1);
        } else {
            printf("Second file is a super set of first file and first 16 bytes of extra chars are..\n");
            printDiffChars(t2+size1,t2,size2);
        }
    }
    else if(neq!=NULL){
        //if diff point is detected
        printf("first file split point starting at offset(# of bytes) : %ld and char: %c\n",(neq[0]-map1),*neq[0]);
        printf("second file split point starting at offset(# of bytes): %ld and char: %c\n",(neq[1]-map2),*neq[1]);
        printDiffChars(neq[0],t1,size1);
        printDiffChars(neq[1],t2,size2);

        free(neq);
    } else {
        printf("The files are identical \n");
    }
    //un-map the mapped files
    close(f1);
    close(f2);
    munmap(map1,size1);
    munmap(map2,size2);
    end=clock();
    double time_used=(((double)(end-start))/CLOCKS_PER_SEC);
    printf("Time taken is %f seconds or %f ms\n",time_used,time_used*NMSEC_PER_SEC);
    return 0;
}
/*
 * @brief small helper function to given the minimum of two numbers
 * @param num1 first number
 * @param num2 second number
 * @return smaller of the two arguments
 */
long min(long  num1, long num2){
    return (num1 > num2 ) ? num2 : num1;
}

/*
 * @brief Main fucntion that recursively finds the first point where two files differ
 * @returns an array of pointers of split points in both the files
 * @param p1 current pointer in the first file
 * @param p2 current pointer in the second file
 * @param size size of the chunk of the file that is to be compared
 */

char ** findDiffPoint(char *p1,char *p2,long size){
    if(p1==NULL || p2==NULL) return NULL;
    long secon_size=0;
    //check if the size is even , if even we can split the subparts equally
    if(size%2==0) secon_size=size/2;
    else secon_size=(size/2)+1;
    if(memcmp(p1,p2,size/2)==0) {
        if (memcmp(p1 + (size/2), p2 + (size / 2), secon_size) == 0) return NULL;
        else {
            //check if this subpart itself is the start of the mismatched chunk of data
            if (*(p1 + (size/2)) != *(p2 + (size/2))) {
                char **ret = malloc(2 * sizeof(char *));
                if(ret==NULL) {
                    perror("Error allocating the memory");
                    exit(EXIT_FAILURE);
                }
                ret[0] = p1 + (size/2);
                ret[1] = p2 + (size/2);
                return ret;
            }
            //recursive call to the second half
            return findDiffPoint(p1 + (size / 2), p2 + (size / 2), secon_size);
        }
    }else {
        //if first half is not equal , just concentrate on the first half and not worry about second half ,
        //we just have to return the first instance of mismatch
        //check if this subpart itself is the start of the mismatched chunk of data
        if(*(p1)!=*(p2)) {
            char **ret=malloc(2*sizeof(char *));
            if(ret==NULL) {
                perror("Error allocating the memory");
                exit(EXIT_FAILURE);
            }
            ret[0]=p1;
            ret[1]=p2;
            return ret;
        }
        //recursive call to the first half
        return findDiffPoint(p1,p2,size/2);
    }
}
/*
 * @brief print 16 bytes of data from the point of difference if EOF is not encountered.
 *        it stops at the end of file.
 * @param fp the pointer to the start of the memory mapped buffer of file
 * @param size the size of the file
 */
void printDiffChars(char *p,char *fp,long size){
    for(int i=0;i<16;i++){
        if(isEnd(p+i,fp,size)) {printf("%c\nEnd reached before 16 chars\n",*(p+i));
            return;}
        printf("%c",*(p+i));
    }
    printf("\n");
}

/*
 * @brief helper function to see if we have reached end of file by comparing pointer p with the
 *        pointer to last character obtained by adding size of the file to the starting position.
 * @param p current position of the pointer
 * @param fp pointer to the start of memory mapped buffer
 * @param size the size of the file
 */
bool isEnd(char *p,char *fp,long size){
    //check if the pointer passed is indeed the pointer to the last byte of the data
    if(p==fp+(size-1)) return true;
    return false;
}